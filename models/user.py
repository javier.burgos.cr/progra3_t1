from sqlalchemy import Table,Column
from sqlalchemy.sql.sqltypes import Integer, VARCHAR
from config.db import meta

users = Table("users", meta, Column("CodUsuario", Integer, primary_key=True), Column("Nombre", VARCHAR(20)))